//此文件由 Yanyu (周艳宇:zhouyanyus@foxmail.com) 编辑。
;(function () {
    'use strict';
    let taskMaxId,
        taskList;
    init_();
    window.main = {
        add:add,
        del:del,
        update:update,
        cpl:cpl
    };
    /*--------------------🙂功能函数--------------------*/
    //🌶添加函数
    function add(task) {
        if (!task){
            return;
        }
        taskList.push({
            id:taskMaxId + 1,
            title:task,
            cpl:'F',
            createTime:new Date().toLocaleString(),
            finishTime:'',
            updateTime:''
        });
        console.log('taskList:',taskList);
        taskMaxId += 1;
        sync();
        ui.render();
    }
    //🌶删除函数
    function del(id) {
        let idx = findIndex(id);
        if (idx === -1){
            return;
        }
        taskList.splice(idx,1);
        sync();
        ui.render();
    }
    //🌶更新函数
    function update(id,updTask) {
        if (!updTask){
            return;
        }
        let idx = findIndex(id);
        if (idx === -1){
            return;
        }
        taskList[idx].title = updTask;
        sync();
        ui.render();
    }
    //🌶 完成函数
    function cpl(id,updTask) {
        let idx = findIndex(id);
        if (idx === -1){
            return;
        }
        taskList[idx].cpl = updTask;
        if (updTask === 'T'){
            taskList[idx].finishTime = new Date().toLocaleString();
        }else {
            taskList[idx].finishTime = '';
        }
        sync();
        ui.render();
    }

    /*--------------------🙂辅助函数--------------------*/
    function init_() {
        taskMaxId = ms.get('taskMaxId');
        taskList = ms.get('taskList');
        if (!taskMaxId){
            taskMaxId=0;
            sync();
        }
        if (!taskList){
            taskList = [];
            sync();
        }
    }
    function findIndex(id) {
        return taskList.findIndex(function (t) {
            return t.id === id;
        })
    }
    function sync() {
        ms.set('taskMaxId',taskMaxId);
        ms.set('taskList',taskList);
    }

})();