//此文件由 Yanyu (周艳宇:zhouyanyus@foxmail.com) 编辑。
;(function () {
    'use strict';
    /*------------------💋全局💋------------------*/
    window.ui = {
        render: render
    };
    init();

    /*------------------💋渲染💋------------------*/
    function render() {
        document.querySelector('#taskList').innerHTML = '';
        document.querySelector('#taskListC').innerHTML = '';
        document.querySelector('#task').value = '';
        document.querySelector('#id').value = '';
        let list = ms.get('taskList');
        list.forEach(function (t) {
            if (t.cpl === 'F') {
                fcRender('taskList', t);
            } else {
                fcRender('taskListC', t);
            }

        });
    }

    /*------------------💋分开渲染💋------------------*/
    function fcRender(el, pack) {
        let delBtn,
            cplBtn,
            updBtn;
        //🌶 插入一个容器
        let cell = document.createElement('div');
        cell.classList.add('task-item', 'tasking');
        //🌶 插入内容
        if (el === 'taskListC') {
            cell.innerHTML = `
            <form class="renderForm">
                <button id="cplBtn-${pack.id}" class="btn"><i class="fa fa-check-circle icon-i"></i></button>
                <button id="delBtn-${pack.id}" class="btn"><i class="fa fa-trash icon-i"></i></button>
                <button id="updBtn-${pack.id}" class="btn"><i class="fa fa-edit icon-i"></i></button>
                <span style="text-decoration: line-through;color: #aaa">
                    ${pack.title}
                    <span style="float: right;text-decoration: line-through;color: #aaa;font-size: xx-small">完成时间:${pack.finishTime}</span>
                </span>
            </form>
            `;
        } else {
            cell.innerHTML = `
            <form class="renderForm">
                <button id="cplBtn-${pack.id}" class="btn"><i class="fa fa-check-circle-o"></i></button>
                <button id="delBtn-${pack.id}" class="btn"><i class="fa fa-trash"></i></button>
                <button id="updBtn-${pack.id}" class="btn"><i class="fa fa-edit"></i></button>
                <span>
                    ${pack.title}
                    <span style="float: right;font-size: xx-small">添加时间:${pack.createTime}</span>
                </span>
            </form>
            `;
        }
        cplBtn = cell.querySelector('#cplBtn-' + pack.id);
        delBtn = cell.querySelector('#delBtn-' + pack.id);
        updBtn = cell.querySelector('#updBtn-' + pack.id);
        cpl(cplBtn, pack);
        del(delBtn, pack.id);
        upd(updBtn, pack);
        document.querySelector('#' + el).appendChild(cell);
    }

    /*------------------💋增加💋------------------*/
    function add() {
        document.querySelector('#taskForm').addEventListener('submit', function (e) {
            e.preventDefault();
            let inp = document.querySelector('#task');
            let id = document.querySelector('#id');
            if (!id.value) {
                main.add(inp.value);
            } else {
                main.update(parseInt(id.value), inp.value);
            }
        });
    }

    /*------------------💋删除💋------------------*/
    function del(el, id) {
        el.addEventListener('click', function (e) {
            e.preventDefault();
            main.del(id);
        })
    }

    /*------------------💋更新💋------------------*/
    function upd(el, pack) {
        el.addEventListener('click', function (e) {
            document.querySelector('#taskForm').style.display = 'block';
            document.getElementById('task').focus();
            document.querySelector('#task').setAttribute('placeholder', '请输入您要更新的内容！');
            e.preventDefault();
            document.querySelector('#task').value = pack.title;
            document.querySelector('#id').value = pack.id;
        })
    }

    /*------------------💋完成💋------------------*/
    function cpl(el, t) {
        el.addEventListener('click', function (e) {
            e.preventDefault();
            if (t.cpl === 'T') {
                t.cpl = 'F';
            } else {
                t.cpl = 'T';
            }
            main.cpl(t.id, t.cpl);
        })
    }

    /*------------------💋初始化💋------------------*/
    function init() {
        render();
        add();
    }
})();